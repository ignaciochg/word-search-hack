#!/usr/bin/env python3 

import sys
import json

with open(sys.argv[1],"r") as f:
    comments = json.load(f)

for com in comments:
    print(com["created_at"], com["owner"]["username"],com["text"].replace("\n"," "))


