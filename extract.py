#!/usr/bin/env python3 

# import the necessary packages
from PIL import Image
import pytesseract
from pytesseract import Output
import argparse
import cv2
import os
import sys
import time 

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
   help="path to input image to be OCR'd")
ap.add_argument("-o", "--output", required=True,
   help="path to text")
ap.add_argument("-p", "--preprocess", type=str, default="thresh",
   help="type of preprocessing to be done")
ap.add_argument("-t", "--temp", type=str, default="temp",
   help="temporary file path to store temp picture")
ap.add_argument("-c", "--min-conf", type=int, default=0,
   help="mininum confidence value to filter weak text detection")
args = vars(ap.parse_args())


# load the example image and convert it to grayscale
image = cv2.imread(args["image"])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# check to see if we should apply thresholding to preprocess the
# image
if args["preprocess"] == "thresh":
   gray = cv2.threshold(gray, 0, 255,
      cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
# make a check to see if median blurring should be done to remove
# noise
elif args["preprocess"] == "blur":
   gray = cv2.medianBlur(gray, 3)
# write the grayscale image to disk as a temporary file so we can
# apply OCR to it
cv2.imwrite("{}1.png".format(args["temp"]), gray)



# text = pytesseract.image_to_string(Image.open(filename))
# # os.remove(filename)
# print(text)
# # show the output images
# cv2.imshow("Image", image)
# cv2.imshow("Output", gray)
# cv2.waitKey(0)



def minMax(data):
   first = True

   for d in data:
      x = d["left"]
      y = d["top"]
      w = d["width"]
      h = d["height"]

      if first:
         first = False
         xmin = x
         xmax = x
         ymin = y
         ymax = y
      else:
         if x < xmin:
            xmin = x
         if x > xmax:
            xmax = x
         if y < ymin:
            ymin = y
         if y > ymax:
            ymax = y

   return (xmin,ymin),(xmax,ymax)

def bucketIt(data, rows, cols, min_v,max_v):
   xmin,ymin = min_v
   xmax,ymax  = max_v

   x = d["left"] - xmin
   y = d["top"] - ymin

   x_factor = rows/(xmax - xmin)
   y_factor = cols/(ymax - ymin)
   # print("mod {} {}".format(x_mod, y_mod))
   bucket_x =  (x*x_factor)%(rows+1)
   bucket_y =  (y*y_factor)%(cols+1)

   return int(bucket_x), int(bucket_y)

image = cv2.imread("{}1.png".format(args["temp"]))
rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
results = pytesseract.image_to_data(rgb, output_type=Output.DICT)

# loop over each of the individual text localizations
data = list()
for i in range(0, len(results["text"])):
   # extract the bounding box coordinates of the text region from
   # the current result
   x = results["left"][i]
   y = results["top"][i]
   w = results["width"][i]
   h = results["height"][i]
   d = dict()
   d["left"] = x
   d["top"] = y
   d["width"] = w
   d["height"] = h
   d["text"] = results["text"][i]
   d["conf"] = results["conf"][i] 
   data.append(d)

# remove the ones that dont count
data = [d for d in data if int(d["conf"])>args["min_conf"]]


data = sorted(data, key = lambda i: (i['top'], i['left'])) 
rows= int(input("#rows: "))
cols = int(input("#cols: "))
min_v, max_v = minMax(data)
# print("{}x{}".format(min_v,max_v))



for d in data:

   pos_x, pos_y = bucketIt(d, rows,cols,min_v, max_v )
   d["pos_x"] = pos_x
   d["pos_y"] = pos_y

data = sorted(data, key = lambda i: (i['pos_y'], i['pos_x'])) 


# data_rows = []
# temp = []
# current_y = 0
# for d in data:
#    pos_x =d["pos_y"]
#    if current_y == pos_y:
#       temp.append(f)
#    else:
#       current_y = pos_y
#       data_rows.append(temp)
#       temp = []
#       temp.append(d)
# data_rows.append(temp) # last one not aded automatically
# for rw in data_rows:
#    current = 0
#    for d in rw:
#       if d["pos_y"] == current:
#          sys.stdout.write()

outfile = "{}.txt".format(args["output"])

with open(outfile, "w") as outF:
   current_y = 0
   for i_order,d in enumerate(data): 
      x = d["left"]
      y = d["top"]
      w = d["width"]
      h = d["height"]
      pos_x =d["pos_x"]
      pos_y = d["pos_y"]
      # extract the OCR text itself along with the confidence of the
      # text localization
      text = d["text"]
      conf = int(d["conf"])


      text = "".join([c if ord(c) < 128 else "" for c in text]).strip()

      # # print(x,y)
      # print(current_y, pos_y)
      if current_y == pos_y:
         outF.write(text)
         sys.stdout.write(text)
      else:
         current_y = pos_y
         outF.write("\n{}".format(text))
         sys.stdout.write("\n{}".format(text))
      # print("{} {}x{} {}".format(i_order,d["pos_y"], d["pos_x"],text.upper()))
      # print("Confidence: {}".format(conf))
      # print("Text: {}".format(text))
      # print("")


      cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
      cv2.putText(image, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX,
         1.2, (0, 0, 255), 3)
      # show the output image
   print()
   outF.write("\n")

time.sleep(2)
cv2.imshow("Image", image)
cv2.imwrite("{}2.png".format(args["temp"]), image)
cv2.waitKey(0)

