# dependencies
tesseract-ocr, via pacman apt or homebrew
pip requirements.txt



# isntall tesseract 
## mac 
https://github.com/tesseract-ocr/tesseract/wiki#homebrew
https://formulae.brew.sh/formula/tesseract

## manjaro 
```bash
sudo pacman -S tesseract
sudo pacman -S tesseract-data-eng
sudo pacman -S tesseract-data-spa

```

# instagram downloader doc
https://pypi.org/project/instalooter/
https://instalooter.readthedocs.io/en/latest/examples.html
https://instalooter.readthedocs.io/en/latest/instalooter/looters.html#instalooter.looters.ProfileLooter