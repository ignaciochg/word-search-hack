#!/usr/bin/env python3 

from itertools import *
import numpy as np 
import sys
import json
import unidecode 

def printGame(data):
   for r in data:
      print("{}{}".format("\t"," ".join(r)))


def genSubset(myString):
   r = [myString[x:y] for x, y in combinations(range(len(myString) + 1), r = 2)] 
   return r


def getRowSubs(data, disable_subgen=False):
   result = []
   for row in data:
      if disable_subgen:
         result.append(row)
      else:
         result = result + genSubset(row)
   return result

def strToList(myString):
   return [char for char in myString]

def printPossible(possible):
   for p in possible:
      print(p)


def reversePossible(possible):
   result = []
   for p in possible:
      result.append("".join(reversed(p)))
   return result

def uniq(arr):
   return list(set(arr))


def getFromUser():
   data = []
   for x in range(row):
      r = input("fila[{}]: ".format(x+1)).strip()
      while len(r) != col:
         r = input("fila[{}]: ".format(x+1)).strip()
      data.append(r.upper())
   return data

def getFromFile(row,col):
   with open(input("nombre de archivo: ").strip(),"r") as f:
      lines = f.readlines() 
   dat = [l.strip() for l in lines]

   if len(dat) != row:
      sys.exit("archivo contiene {} filas, pero {} fueron especificadas.".format(len(dat),row))

   for d in dat:
      if len(d) != col:
         sys.exit("una/multiple columna contiene {} letras, pero {} fueron especificadas.".format(len(d),cols))

   return dat


def getBooleanInput(msg="", defaultBool=None):
   i = input(msg).strip().lower()
   if defaultBool != None and i.strip() == "":
      return defaultBool
   while True:
      if i == "s" or i == "si" or i == "true" or i == "t":
         return True
      elif i == "n" or i == "no" or i == "false" or i == "f":
         return False
      i = input(msg).strip().lower()


if __name__ == '__main__':
   row = int(input("# filas: "))
   col = int(input("# columnas: "))

   print("Sopa de letras: {}x{}".format(row,col))
   

   use_file = getBooleanInput("Importar archivo (s/n)? ")
   if use_file:
      data = getFromFile(row,col)
   else:
      data = getFromUser()

   # for d in data:
   #    print(type(d))
   

   printGame(data)

   filter_dic = getBooleanInput("filtrar usando diccionario (s/n)? ")

   # row words
   possible = getRowSubs(data, filter_dic)
   
   # columns words
   collWords = getRowSubs(np.array([strToList(r) for r in data]).T, filter_dic)
   for col in collWords:
      possible.append("".join(col))


   




   # diagonals up and down 
   x,y = row, col
   # x,y = 4,5
   a = np.array([strToList(r) for r in data])
   # a = np.array(
   #          [[-2,  5,  3,  2,4],
   #           [ 9, -6,  5,  1,3],
   #           [ 3,  2,  7,  3,2],
   #           [-1,  8, -4,  8,1]])

   # create a default array of specified dimensions
   # a = np.arange(x*y).reshape(x,y)

   diags = [a[::-1,:].diagonal(i) for i in range(-a.shape[0]+1,a.shape[1])]
   diags.extend(a.diagonal(i) for i in range(a.shape[1]-1,-a.shape[0],-1))


   for d in diags:
      possible.append("".join(list(d)))


   # reverse possible wirds 
   possible = possible + reversePossible(possible)

   # remove repeated
   possible = uniq(possible)


   if not filter_dic and getBooleanInput("ordenar alfabeticamente (s/n)? "):
      possible = sorted(possible)


   if filter_dic:
      temp = []
      dic_loc = input("nombre archive dicionario: ")
      with open(dic_loc, "r") as diccion:
         dic = json.loads(diccion.read())
         dic = [unidecode.unidecode(di).upper() for di in dic]
      for d in dic:
         for pos in possible:
            if d in pos:
               temp.append(d)
            if d.replace(" ","") in pos: # checking if removing space makes a difference
               temp.append(d)
      possible = sorted(list(set(temp)))


   guardar = getBooleanInput("guardar lista a archivo (s/n)? ")
   if guardar:
      with open(input("nombre archivo para salvar: ").strip(), "w") as f:
         for w in possible:
            f.write("{}\n".format(w))

   print("####################################")

   printPossible(possible)


   
